﻿using LoginVsi.Accounts;
using LoginVsi.Accounts.Clients.Accounts;
using LoginVsi.Accounts.Credentials;
using LoginVsi.Infrastructures.Clients.Environments;
using LoginVsi.Infrastructures.Clients.Gateways;
using LoginVsi.Infrastructures.Environments;
using LoginVsi.Infrastructures.Gateways;
using LoginVsi.LauncherLeasing.Launchers.Properties;
using LoginVsi.LauncherRegistry.Launchers;
using LoginVsi.SessionRequests.Clients.Requests;
using LoginVsi.SessionRequests.Requests;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using TestCases.Base;
using Xunit;
using LauncherLeasingNewLauncher = LoginVsi.LauncherLeasing.Launchers.NewLauncher;

namespace TestCases.Scenario2
{
    public class AttempFailureWhenConnectionEndedWithErrors : TestBase
    {
        [Fact]
        public async Task PrepareSystem()
        {
            var environmentsService = ServiceProvider.GetService<IEnvironmentsService>();
            var gatewaysService = ServiceProvider.GetService<IGatewaysService>();
            var accountsService = ServiceProvider.GetService<IAccountsService>();
            var sessionRequestsService = ServiceProvider.GetService<ISessionRequestsService>();

            var environmentResponse = await environmentsService.CreateEnvironment(CreateEnvironment());
            var gatewayResponse = await gatewaysService.CreateGateway(environmentResponse.Resource.EnvironmentId, CreateGateway());
            await accountsService.CreateAccount(CreateAccount(), "domain");
            
            var sessionRequestResponse =
                await sessionRequestsService.CreateSessionRequest(CreateSessionRequest(), gatewayResponse.Resource.GatewayId);
        }

        private static NewLauncher CreateLauncher()
        {
            return new NewLauncher
            {
                ComputerName = "PC",
                Fqdn = "Standalone"
            };
        }

        private static LauncherLeasingNewLauncher CreateLauncherLeasingNewLauncher(Guid launcherId)
        {
            return new LauncherLeasingNewLauncher
            {
                LauncherId = launcherId,
                Properties = new[] {new NewLauncherProperty {PropertyId = "p1", Value = "1"}}
            };
        }

        private static NewSessionRequest CreateSessionRequest()
        {
            return new NewSessionRequest
            {
                AccountGroups = new string[] { },
                LauncherGroups = new string[] { }
            };
        }

        private static NewAccount CreateAccount()
        {
            return new NewAccount
            {
                LoginCredentials = new NewLoginCredentials
                {
                    Password = "password"
                },
                Username = "loginpi"
            };
        }

        private static NewGateway CreateGateway()
        {
            return new NewGateway
            {
                Name = "gateway1",
                Description = "description",
                GatewayUrl = new Uri("http://172.23.108.88"),
                GatewayTypeId = "rdp"
            };
        }

        private static NewEnvironment CreateEnvironment()
        {
            return new NewEnvironment
            {
                CategoryId = "test",
                Description = "description",
                Name = "name",
                VendorId = "microsoft"
            };
        }
    }
}
