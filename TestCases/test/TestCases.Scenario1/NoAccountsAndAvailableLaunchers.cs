﻿using LoginVsi.AccountLeasing.Clients.Leases;
using LoginVsi.Infrastructures.Clients.Environments;
using LoginVsi.Infrastructures.Clients.Gateways;
using LoginVsi.Infrastructures.Environments;
using LoginVsi.Infrastructures.Gateways;
using LoginVsi.LauncherLeasing.Clients.Launchers.States;
using LoginVsi.LauncherLeasing.Clients.Leases;
using LoginVsi.LauncherLeasing.Clients.Leases.States;
using LoginVsi.LauncherLeasing.Constants.Launchers;
using LoginVsi.LauncherLeasing.Constants.Leases;
using LoginVsi.LauncherLeasing.Launchers.States;
using LoginVsi.LauncherRegistry.Clients.Launchers;
using LoginVsi.LauncherRegistry.Launchers;
using LoginVsi.SessionRequests.Clients.Requests;
using LoginVsi.SessionRequests.Clients.Requests.Events;
using LoginVsi.SessionRequests.Clients.Requests.States;
using LoginVsi.SessionRequests.Constants.Requests;
using LoginVsi.SessionRequests.Requests;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using TestCases.Base;
using Xunit;

namespace TestCases.Scenario1
{
    public class NoAccountsAndAvailableLaunchers : TestBase
    {
        [Fact]
        public async Task It_Should_SetRequestToFailed_WhenNoAccountLeasesAvailable()
        {
            var sessionRequestEventsService = ServiceProvider.GetService<ISessionRequestEventsReadOnlyService>();
            var sessionRequestStateService = ServiceProvider.GetService<ISessionRequestStatesReadOnlyService>();
            var accountLeasesQueryService = ServiceProvider.GetService<IAccountLeasesQueryService>();
            var launcherLeasesService = ServiceProvider.GetService<ILauncherLeaseReadOnlyService>();
            var launcherService = ServiceProvider.GetService<ILaunchersService>();
            var launcherStateService = ServiceProvider.GetService<ILauncherStateService>();
            var launcherLeaseStateService = ServiceProvider.GetService<ILauncherLeaseStateReadOnlyService>();

            var newLauncher = new NewLauncher
            {
                ComputerName = "test",
                Fqdn = "fqdn"
            };

            var launcherResponse = await launcherService.Create(newLauncher);

            await Task.Delay(TimeSpan.FromSeconds(2)); // Wait for launcher being synced to launcher leasing

            await launcherStateService.UpdateLauncherState(
                new LauncherStateUpdate{State = LauncherStates.Enabled}, launcherResponse.Resource.LauncherId);

            var (environmentId, sessionRequestId) = await CreateSessionRequest();

            await Task.Delay(TimeSpan.FromSeconds(12));

            // Assert that launcher lease was created, but account lease was not

            var accountLeases = await accountLeasesQueryService.GetCurrentAccountLeasesByEnvironment(environmentId);
            var launcherLeases = await launcherLeasesService.GetLeasesByEnvironment(environmentId);

            Assert.Empty(accountLeases);
            Assert.Single(launcherLeases);

            // Assert that launcher lease was cancelled

            var launcherLeaseState = await launcherLeaseStateService.GetLeaseStateById(launcherLeases[0].LeaseId);

            Assert.True(launcherLeaseState.State == LauncherLeaseStates.Cancelled);

            // Assert that SessionRequestWorker created insuficcient accounts event

            var sessionRequestEvents =
                await sessionRequestEventsService.GetSessionRequestEventsBySessionRequest(sessionRequestId);

            Assert.Contains(sessionRequestEvents, x => x.EventType == SessionRequestEvents.InsufficientAccounts);

            // Assert that request got state to failed

            var sessionRequestsState = await sessionRequestStateService.GetSessionRequestStateById(sessionRequestId);

            Assert.True(sessionRequestsState.State == SessionRequestStates.RequestFailed);

            await launcherStateService.UpdateLauncherState(
                new LauncherStateUpdate { State = LauncherStates.Disabled }, launcherResponse.Resource.LauncherId);
        }

        private async Task<(Guid, Guid)> CreateSessionRequest()
        {
            var environmentsService = ServiceProvider.GetService<IEnvironmentsService>();
            var gatewaysService = ServiceProvider.GetService<IGatewaysService>();
            var sessionRequestsService = ServiceProvider.GetService<ISessionRequestsService>();

            var newEnvironment = new NewEnvironment
            {
                VendorId = "Microsoft",
                CategoryId = "test",
                Name = Guid.NewGuid().ToString(),
                Description = "description"
            };

            var environmentResponse = await environmentsService.CreateEnvironment(newEnvironment);
            var environmentId = environmentResponse.Resource.EnvironmentId;

            var newGateway = new NewGateway
            {
                GatewayTypeId = "rdp",
                Description = "description",
                GatewayUrl = new Uri("http://10.10.10.10"),
                Name = Guid.NewGuid().ToString()
            };

            var gatewayResponse = await gatewaysService.CreateGateway(environmentId, newGateway);
            var gatewayId = gatewayResponse.Resource.GatewayId;

            var newSessionRequest = new NewSessionRequest();
            var sessionRequestResponse =
                await sessionRequestsService.CreateSessionRequest(newSessionRequest, gatewayId);

            return (environmentId, sessionRequestResponse.Resource.RequestId);
        }
    }
}