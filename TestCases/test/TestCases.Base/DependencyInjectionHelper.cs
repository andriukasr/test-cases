﻿using LoginVsi.AccountLeasing.Clients;
using LoginVsi.Accounts.Clients;
using LoginVsi.Basics.Extensions.Uris;
using LoginVsi.Basics.Logging;
using LoginVsi.Infrastructures.Clients;
using LoginVsi.LauncherLeasing.Clients;
using LoginVsi.LauncherRegistry.Clients;
using LoginVsi.Network.Services;
using LoginVsi.Network.Services.Authenticated;
using LoginVsi.Network.Services.Authentication;
using LoginVsi.SessionRequests.Clients;
using LoginVsi.Workers.DI.Setup;
using Microsoft.Extensions.DependencyInjection;
using System;
using TestCases.DI.Setup;

namespace TestCases.Base
{
    public class DependencyInjectionHelper
    {
        public static IServiceProvider Configure()
        {
            var iocContainer = new ServiceCollection();

            var piUri = new Uri("https://localhost.loginvsi.com:3000");

            var infrastructuresSettings = new InfrastructuresClientSettings(piUri.Append("/infrastructures"));
            var sessionRequestsSettings = new SessionRequestsClientSettings(piUri.Append("/sessionRequests"));
            var accountLeasingSettings = new AccountLeasingClientSettings(piUri.Append("/accountLeasing"));
            var launcherLeasingSettings = new LauncherLeasingClientSettings(piUri.Append("/launcherLeasing"));
            var accountsSettings = new AccountsClientSettings(piUri.Append("/accounts"));
            var launchersSettings = new LaunchersClientSettings(piUri.Append("/launchers"));

            iocContainer
                .AddSingleton<IInfrastructuresClientSettings>(infrastructuresSettings)
                .AddSingleton<ISessionRequestsClientSettings>(sessionRequestsSettings)
                .AddSingleton<IAccountLeasingClientSettings>(accountLeasingSettings)
                .AddSingleton<ILauncherLeasingClientSettings>(launcherLeasingSettings)
                .AddSingleton<IAccountsClientSettings>(accountsSettings)
                .AddSingleton<ILaunchersClientSettings>(launchersSettings);

            var provider = iocContainer
                .AddWorkerDependencies()
                .AddSingleton<ILogger, ConsoleLogger>()
                .AddSingleton<IHttpReadOnlyService, JsonHttpService>()
                .AddSingleton<IHttpService, JsonHttpService>()
                .AddSingleton<IAuthenticationSettings, AuthenticationSettings>()
                .AddAllClientsDependencies()
                .BuildServiceProvider();

            return provider;
        }
    }
}