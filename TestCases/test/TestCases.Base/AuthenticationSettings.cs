using LoginVsi.Network.Services.Authentication;

namespace TestCases.Base
{
    public class AuthenticationSettings : IAuthenticationSettings
    {
        public AuthenticationSettings()
        {
            Id = "Worker";
            Secret = "4BP7E9U775P762MH46QSW4H2GLCATHACOOH8JYUE";
            Authority = "https://localhost.loginvsi.com:3000/identityserver";
            Scope = "microservice";
        }

        public string Id { get; }

        public string Secret { get; }

        public string Authority { get; }

        public string Scope { get; }
    }
}