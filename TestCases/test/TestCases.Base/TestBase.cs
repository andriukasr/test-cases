﻿using LoginVsi.Network.Services.Authentication;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reactive.Linq;

namespace TestCases.Base
{
    public abstract class TestBase
    {
        protected readonly IServiceProvider ServiceProvider;

        protected TestBase()
        {
            ServiceProvider = DependencyInjectionHelper.Configure();

            var authenticationService = ServiceProvider.GetService<IAuthenticationService>();

            authenticationService.Logon().GetAwaiter().GetResult();
        }
    }
}