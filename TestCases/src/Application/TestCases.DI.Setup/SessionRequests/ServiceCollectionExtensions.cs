using LoginVsi.SessionRequests.Clients.Requests;
using LoginVsi.SessionRequests.Clients.Requests.Events;
using LoginVsi.SessionRequests.Clients.Requests.States;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.SessionRequests
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSessionRequestsClientDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<ISessionRequestsReadOnlyService, SessionRequestsReadOnlyService>()
                .AddSingleton<ISessionRequestsReadOnlyServiceInternals, SessionRequestsReadOnlyService>()
                .AddSingleton<ISessionRequestsQueryService, SessionRequestsQueryService>()
                .AddSingleton<ISessionRequestsService, SessionRequestsService>()
                
                .AddSingleton<ISessionRequestEventsReadOnlyService, SessionRequestEventsReadOnlyService>()
                .AddSingleton<ISessionRequestEventsReadOnlyServiceInternals, SessionRequestEventsReadOnlyService>()
                .AddSingleton<ISessionRequestEventsService, SessionRequestEventsService>()
                
                .AddSingleton<ISessionRequestStatesReadOnlyService, SessionRequestStatesReadOnlyService>();
        }
    }
}