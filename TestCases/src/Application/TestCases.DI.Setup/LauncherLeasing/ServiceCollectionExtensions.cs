using LoginVsi.LauncherLeasing.Clients.Launchers;
using LoginVsi.LauncherLeasing.Clients.Leases;
using LoginVsi.LauncherLeasing.Clients.Leases.States;
using LoginVsi.LauncherRegistry.Clients.Launchers.Events;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.LauncherLeasing
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLauncherLeasingClientDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<ILauncherService, LauncherService>()
                .AddSingleton<ILauncherReadOnlyServiceInternals, LauncherReadOnlyService>()
                .AddSingleton<ILauncherLeaseReadOnlyService, LauncherLeaseReadOnlyService>()
                .AddSingleton<ILauncherLeaseStateReadOnlyService, LauncherLeaseStateReadOnlyService>()
                .AddSingleton<ILauncherEventsReadOnlyServiceInternals, LauncherEventsReadOnlyService>()
                .AddSingleton<ILauncherEventsService, LauncherEventsService>();
        }
    }
}