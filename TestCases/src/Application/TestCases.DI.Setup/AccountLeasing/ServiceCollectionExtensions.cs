using LoginVsi.AccountLeasing.Clients.Leases;
using LoginVsi.AccountLeasing.Clients.Leases.States;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.AccountLeasing
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAccountLeasingClientDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<IAccountLeasesQueryService, AccountLeasesQueryService>()
                .AddSingleton<IAccountLeaseStateReadOnlyService, AccountLeaseStateReadOnlyService>();
        }
    }
}