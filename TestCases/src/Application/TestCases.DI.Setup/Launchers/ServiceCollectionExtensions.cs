using LoginVsi.LauncherLeasing.Clients.Launchers.States;
using LoginVsi.LauncherRegistry.Clients.Launchers;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.Launchers
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLauncherClientDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<ILaunchersService, LaunchersService>()
                .AddSingleton<ILaunchersReadOnlyService, LaunchersReadOnlyService>()
                .AddSingleton<ILaunchersReadOnlyServiceInternals, LaunchersReadOnlyService>()
                .AddSingleton<ILauncherStateService, LauncherStateService>()
                .AddSingleton<ILauncherStateReadOnlyService, LauncherStateReadOnlyService>();
        }
    }
}