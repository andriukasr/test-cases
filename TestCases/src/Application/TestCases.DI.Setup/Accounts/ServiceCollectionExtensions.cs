using LoginVsi.Accounts.Clients.Accounts;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.Accounts
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAccountsClientDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<IAccountsReadOnlyService, AccountsReadOnlyService>()
                .AddSingleton<IAccountsReadOnlyServiceInternals, AccountsReadOnlyService>()
                .AddSingleton<IAccountsService, AccountsService>();
        }
    }
}