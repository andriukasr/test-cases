using LoginVsi.Infrastructures.Clients.Environments;
using LoginVsi.Infrastructures.Clients.Gateways;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.Infrastructures
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructuresClientDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<IEnvironmentsReadOnlyService, EnvironmentsReadOnlyService>()
                .AddSingleton<IEnvironmentsReadOnlyServiceInternals, EnvironmentsReadOnlyService>()
                .AddSingleton<IEnvironmentsService, EnvironmentsService>()
                .AddSingleton<IGatewaysReadOnlyService, GatewaysReadOnlyService>()
                .AddSingleton<IGatewaysReadOnlyServiceInternals, GatewaysReadOnlyService>()
                .AddSingleton<IGatewaysService, GatewaysService>();
        }
    }
}