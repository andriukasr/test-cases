﻿using LoginVsi.LauncherRegistry.Clients.Launchers;
using Microsoft.Extensions.DependencyInjection;

namespace TestCases.DI.Setup.LauncherRegistry
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLauncherRegistryClientsDependencies(this IServiceCollection services)
        {
            return services
                .AddSingleton<ILaunchersReadOnlyServiceInternals, LaunchersReadOnlyService>()
                .AddSingleton<ILaunchersService, LaunchersService>();
        }
    }
}