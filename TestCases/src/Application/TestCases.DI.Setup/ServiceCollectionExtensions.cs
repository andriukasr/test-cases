using Microsoft.Extensions.DependencyInjection;
using TestCases.DI.Setup.AccountLeasing;
using TestCases.DI.Setup.Accounts;
using TestCases.DI.Setup.Infrastructures;
using TestCases.DI.Setup.LauncherLeasing;
using TestCases.DI.Setup.Launchers;
using TestCases.DI.Setup.SessionRequests;

namespace TestCases.DI.Setup
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddAllClientsDependencies(this IServiceCollection services)
        {
            return services
                .AddSessionRequestsClientDependencies()
                .AddInfrastructuresClientDependencies()
                .AddAccountLeasingClientDependencies()
                .AddLauncherLeasingClientDependencies()
                .AddAccountsClientDependencies()
                .AddLauncherClientDependencies();
        }
    }
}